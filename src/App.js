import React, { Component } from 'react';
class App extends Component {
  state = {
    // arr: [],
    projectlist: [
      {
        id: 1,
        title: "BankStatement",
        complete: true,
      },

      {
        id: 2,
        title: "BankCheque",
        complete: true
      },
      {
        id: 3,
        title: "bankdetails",
        complete: true
      }
    ]
  }
  // uisng the map function in details 
  render() {
    return (
      <div className="App">
        <h2>Map function of the completing of the courses </h2>
        {this.state.projectlist.map((item)=>{
          return(
            <div key={item.id}>
              {item.title}
            </div>
          )

        })
        }
      </div>
    )
  }
}
export default App;